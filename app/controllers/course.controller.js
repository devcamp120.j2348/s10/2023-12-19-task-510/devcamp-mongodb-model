const mongoose = require("mongoose");
const courseModel = require("../models/course.model");

const createCourse = async (req, res) => {
    try {
        //B1: Chuẩn bị dữ liệu
        const { reqTitle, reqDescription, reqStudent } = req.body;
        // { reqTitle: 'R48', reqDescription: 'NodeJS - ReactJS', reqStudent: 15 }

        //B2: Validate dữ liệu
        if(!reqTitle) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Title is required!"
            })
        }

        if(!(Number.isInteger(reqStudent) && reqStudent >= 0)) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Count student is not valid!"
            })
        }

        //B3: Thao tác với CSDL
        const newCourse = {
            _id: new mongoose.Types.ObjectId(),
            title: reqTitle,
            description: reqDescription,
            noStudent: reqStudent
        }

        const result = await courseModel.create(newCourse);

        return res.status(200).json({
            status: "Create course successfully",
            data: result
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getAllCourse = async (req, res) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với CSDL
    try {
        const result = await courseModel.find();

        return res.status(200).json({
            status: "Get all course successfully",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getCourseById = async (req, res) => {
    try {
        //B1: Chuẩn bị dữ liệu
        const courseId = req.params.courseid;
        //B2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(courseId)) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Course ID is not valid!"
            })
        }
        //B3: Thao tác với CSDL
        const result = await courseModel.findById(courseId);

        if(result) {
            return res.status(200).json({
                status: "Get course successfully",
                data: result
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const updateCourseById = async (req, res) => {
    try {
        //B1: Chuẩn bị dữ liệu
        const courseId = req.params.courseid;
        const { reqTitle, reqDescription, reqStudent } = req.body;
        //B2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(courseId)) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Course ID is not valid!"
            })
        }

        if(reqStudent !== undefined && !reqTitle) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Title is required!"
            })
        }

        if(reqStudent !== undefined && !(Number.isInteger(reqStudent) && reqStudent >= 0)) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Count student is not valid!"
            })
        }

        //B3: Thao tác với CSDL
        var courseUpdate = {};

        if(reqTitle !== undefined) { courseUpdate.title = reqTitle };
        if(reqDescription !== undefined) { courseUpdate.description = reqDescription };
        if(reqStudent !== undefined) { courseUpdate.noStudent = reqStudent };
        
        const result = await courseModel.findByIdAndUpdate(courseId, courseUpdate);

        if(result) {
            return res.status(200).json({
                status: "Update course successfully",
                data: result
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
  
}

const deleteCourseById = async (req, res) => {
    try {
        //B1: Chuẩn bị dữ liệu
        const courseId = req.params.courseid;
        //B2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(courseId)) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Course ID is not valid!"
            })
        }
        //B3: Thao tác với CSDL
        const result = await courseModel.findByIdAndDelete(courseId);

        if(result) {
            return res.status(200).json({
                status: "Delete course successfully",
                data: result
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}